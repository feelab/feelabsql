BEGIN;

DELETE FROM files f
WHERE NOT EXISTS (SELECT 1
      FROM files_recordings fr
      WHERE fr.file_id = f.file_id
);

COMMIT;
