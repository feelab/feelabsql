CREATE EXTENSION IF NOT EXISTS citext; -- datatype for case insensitive text
CREATE EXTENSION IF NOT EXISTS btree_gist;
-- for making combined index on time range and integer

CREATE OR REPLACE FUNCTION extract_interval(TSTZRANGE) RETURNS INTERVAL AS
$func$
  SELECT UPPER($1) - LOWER($1);
$func$ LANGUAGE SQL STABLE;

-- Birds


CREATE TABLE birds (
    bird_id             integer PRIMARY KEY, -- number in colony spreadsheet
    bird_band           citext, -- e.g. 'blue123'
    bird_name           citext UNIQUE,
    date_hatched        date,
    date_died           date,
    bird_notes          text
);
CREATE INDEX birds_bird_name_ops_index ON birds (bird_name text_pattern_ops);

CREATE TABLE birds_separation (
    bird_id             integer REFERENCES birds PRIMARY KEY,
    date_separated      date NOT NULL
    -- date bird was separated from its parents
);

CREATE FUNCTION in_lifetime(my_bird integer, my_date date)
    RETURNS boolean AS $in_lifetime$
    DECLARE
        birth_date date;
        death_date date;
    BEGIN
        SELECT date_hatched, date_died INTO STRICT birth_date, death_date
            FROM birds WHERE bird_id = my_bird;
        RETURN my_date >= birth_date AND my_date <= death_date;
    END;
$in_lifetime$ LANGUAGE plpgsql;

CREATE FUNCTION date_separated_trg()
    RETURNS trigger AS $date_separated_trg$
    BEGIN
        IF (NOT in_lifetime(NEW.bird_id, NEW.date_separated)) THEN
            RAISE EXCEPTION 'date of separation is not during birds lifetime';
        END IF;
        RETURN NEW;
    END;
$date_separated_trg$ LANGUAGE plpgsql;

CREATE TRIGGER date_separated_trg
    BEFORE INSERT OR UPDATE ON birds_separation
    FOR EACH ROW EXECUTE PROCEDURE date_separated_trg();

CREATE TABLE surgeries (
    bird_id             integer REFERENCES birds PRIMARY KEY,
    experiment_name     citext NOT NULL, -- e.g. 'HVC recording in juveniles'
    surgery_date        date NOT NULL,
    surgery_parameters  jsonb
);
CREATE INDEX surgery_exp_index ON surgeries (experiment_name);
CREATE INDEX surgery_exp_ops_index ON surgeries (experiment_name text_pattern_ops);
CREATE INDEX surgery_surgery_date_index ON surgeries (surgery_date);
CREATE INDEX surgery_exp_parameters_index ON surgeries USING gin (surgery_parameters);

CREATE FUNCTION surgery_date_trg()
    RETURNS trigger AS $surgery_date_trg$
    BEGIN
        IF (NOT in_lifetime(NEW.bird_id, NEW.surgery_date)) THEN
            RAISE EXCEPTION 'date of surgery is not during birds lifetime';
        END IF;
        RETURN NEW;
    END;
$surgery_date_trg$ LANGUAGE plpgsql;

CREATE TRIGGER surgery_date_trg
    BEFORE INSERT OR UPDATE ON surgeries
    FOR EACH ROW EXECUTE PROCEDURE surgery_date_trg();


CREATE TYPE parent AS ENUM('mother', 'father');
CREATE TABLE birds_parentage(
    child_id            integer REFERENCES birds NOT NULL,
    relationship        parent NOT NULL,
    parent_id           integer REFERENCES birds NOT NULL,
    PRIMARY KEY (child_id, relationship)
);
CREATE INDEX birds_parentage_parent_index ON birds_parentage (parent_id);


-- Devices

-- E.g. RHD2164 chip, parylene sheet with 5 um tip
CREATE TABLE device_types (
    device_type_id         serial PRIMARY KEY,
    device_type_name       citext UNIQUE NOT NULL,
    device_type_parameters jsonb
);
CREATE INDEX device_types_param_index ON device_types USING gin (device_type_parameters);
CREATE INDEX device_types_name_ops_index ON device_types (device_type_name text_pattern_ops);

CREATE TABLE device_type_pins (
    pin_id              serial PRIMARY KEY,
    device_type_id      integer REFERENCES device_types NOT NULL,
    pin_name            citext NOT NULL,
    UNIQUE (pin_id, device_type_id),
    UNIQUE (device_type_id, pin_name)
);

-- For devices with discrete subcomponents, e.g. a headstage
CREATE TABLE subcomponents (
    subcomponent_id     serial PRIMARY KEY,
    device_type_id      integer REFERENCES device_types NOT NULL,
    subcomponent_name   citext NOT NULL, -- U1, J1 etc
    UNIQUE (device_type_id, subcomponent_name),
    UNIQUE (subcomponent_id, device_type_id)
);

CREATE TABLE assemblies (
    assembly_id         serial PRIMARY KEY,
    assembly_name       citext UNIQUE NOT NULL
);
CREATE INDEX assemblies_assembly_name_ops_index ON
       assemblies (assembly_name text_pattern_ops);

CREATE TABLE assembly_population (
    population_id         serial PRIMARY KEY,
    assembly_id           integer REFERENCES assemblies NOT NULL,
    device_type_id        integer REFERENCES device_types NOT NULL,
    UNIQUE (assembly_id, population_id, device_type_id), -- For use in foreign keys
    UNIQUE (device_type_id, population_id) -- For use in foreign keys
);
CREATE INDEX assembly_population_assebly_device_type_index ON
    assembly_population (assembly_id, device_type_id);


CREATE TABLE population_hierarchy (
    population_id         integer REFERENCES assembly_population NOT NULL,
    parent_population_id  integer REFERENCES assembly_population NOT NULL,
    parent_device_type_id integer NOT NULL,
    subcomponent_id       integer NOT NULL,
    PRIMARY KEY (population_id, parent_population_id),
    FOREIGN KEY (parent_device_type_id, parent_population_id) REFERENCES
        assembly_population (device_type_id, population_id),
    FOREIGN KEY (subcomponent_id, parent_device_type_id) REFERENCES
        subcomponents (subcomponent_id, device_type_id),
    CHECK (population_id <> parent_population_id)
);
CREATE INDEX hierarchy_parent_dt_index ON
    population_hierarchy (parent_device_type_id, parent_population_id);
CREATE INDEX hierarchy_subcomponent_index ON
    population_hierarchy (subcomponent_id, parent_device_type_id);
CREATE INDEX hierarchy_population_index ON population_hierarchy (parent_population_id);

CREATE OR REPLACE FUNCTION unique_hierarchy_head_trg()
RETURNS trigger AS $unique_head_trg$
DECLARE
    target_assembly_id integer;
    hier_head integer;
BEGIN
    SELECT assembly_id INTO STRICT target_assembly_id
        FROM assembly_population ap
        WHERE ap.population_id = NEW.population_id;
    SELECT DISTINCT pl.parent_population_id INTO STRICT hier_head
        FROM population_hierarchy pl
            INNER JOIN assembly_population ap USING (population_id)
            WHERE ap.assembly_id = target_assembly_id
            AND NOT EXISTS (
                SELECT 1
                FROM population_hierarchy pr
                WHERE pl.parent_population_id = pr.population_id
            );
    RETURN NEW;
EXCEPTION
     WHEN NO_DATA_FOUND THEN -- I don't think this is possible
         RAISE EXCEPTION 'Assembly % has no head?!', target_assembly_id;
     WHEN TOO_MANY_ROWS THEN
         RAISE EXCEPTION 'Assembly % has too many heads', target_assembly_id;
END;
$unique_head_trg$ LANGUAGE plpgsql;

CREATE CONSTRAINT TRIGGER unique_population_head_trg
    AFTER INSERT OR UPDATE ON assembly_population
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE PROCEDURE unique_hierarchy_head_trg();

CREATE CONSTRAINT TRIGGER unique_hierarchy_head_trg
    AFTER INSERT OR UPDATE ON population_hierarchy
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE PROCEDURE unique_hierarchy_head_trg();

-- Adjacency list of pins
CREATE TABLE pin_connections (
    assembly_id         integer,
    from_population_id  integer,
    to_population_id    integer,
    from_device_type_id integer,
    to_device_type_id   integer,
    from_pin_id         integer,
    to_pin_id           integer,
    PRIMARY KEY (
        assembly_id,
        from_population_id,
        to_population_id,
        from_pin_id,
        to_pin_id
    ),
    FOREIGN KEY (assembly_id, from_population_id, from_device_type_id) REFERENCES
        assembly_population (assembly_id, population_id, device_type_id),
    FOREIGN KEY (assembly_id, to_population_id, to_device_type_id) REFERENCES
        assembly_population (assembly_id, population_id, device_type_id),
    FOREIGN KEY (from_pin_id, from_device_type_id) REFERENCES
        device_type_pins (pin_id, device_type_id),
    FOREIGN KEY (to_pin_id, to_device_type_id) REFERENCES
        device_type_pins (pin_id, device_type_id),
    CHECK (from_pin_id <> to_pin_id)
);
CREATE INDEX pin_connections_from_pin_index ON pin_connections (from_pin_id);
CREATE INDEX pin_connections_to_pin_index ON pin_connections (to_pin_id);
CREATE INDEX pin_connections_from_device_index ON pin_connections (from_device_type_id);
CREATE INDEX pin_connections_to_device_index ON pin_connections (to_device_type_id);
CREATE INDEX pin_connections_from_population_index ON pin_connections (from_population_id);
CREATE INDEX pin_connections_to_population_index ON pin_connections (to_population_id);

CREATE FUNCTION reciprocal_pins_trg()
RETURNS trigger AS $reciprocal_pins_trg$
    DECLARE
        target_population_id integer;
        target_device_type_id integer;
        target_pin_id integer;
    BEGIN
        SELECT to_population_id, to_device_type_id, to_pin_id INTO
            target_population_id, target_device_type_id, target_pin_id
            FROM pin_connections pc
            WHERE pc.to_population_id = NEW.from_population_id
                AND pc.from_population_id = NEW.to_population_id
                AND pc.to_device_type_id = NEW.from_device_type_id
                AND pc.from_device_type_id = NEW.to_device_type_id
                AND pc.to_pin_id = NEW.from_pin_id
                AND pc.from_pin_id = NEW.to_pin_id;
        IF (NOT FOUND) THEN
           RAISE EXCEPTION
                 'Connection from population (%), device (%), pin (%) is not reciprocal',
                 target_population_id, target_device_type_id, target_pin_id;
        END IF;
        RETURN NEW;
    END;
$reciprocal_pins_trg$ LANGUAGE plpgsql;

CREATE CONSTRAINT TRIGGER reciprocal_pins_trg
    AFTER INSERT OR UPDATE ON pin_connections
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE PROCEDURE reciprocal_pins_trg();

CREATE TYPE pop_pin  AS (
    pop_id    integer,
    pin_id    integer
);

CREATE TABLE devices (
    device_id           serial PRIMARY KEY,
    device_type_id      integer REFERENCES device_types NOT NULL,
    device_name         citext UNIQUE,
    UNIQUE (device_type_id, device_id)
);
CREATE INDEX devices_device_name_ops_index ON
    devices (device_name text_pattern_ops);

CREATE TABLE assembly_instances (
    assembly_instance_id   serial PRIMARY KEY,
    assembly_id            integer REFERENCES assemblies NOT NULL,
    assembly_instance_name citext UNIQUE NOT NULL,
    UNIQUE (assembly_id, assembly_instance_id)
);
CREATE INDEX assembly_instances_assembly_instance_name_ops_index ON
    assembly_instances (assembly_instance_name text_pattern_ops);

CREATE TABLE assembly_instances_devices (
    assembly_instance_id integer REFERENCES assembly_instances,
    device_id            integer,
    population_id        integer NOT NULL,
    assembly_id          integer NOT NULL,
    device_type_id       integer NOT NULL,
    PRIMARY KEY (assembly_instance_id, device_id),
    FOREIGN KEY (assembly_id, assembly_instance_id) REFERENCES
        assembly_instances (assembly_id, assembly_instance_id),
    FOREIGN KEY (device_type_id, device_id) REFERENCES
        devices (device_type_id, device_id),
    FOREIGN KEY (population_id, assembly_id, device_type_id) REFERENCES
        assembly_population (population_id, assembly_id, device_type_id)
);
CREATE INDEX assembly_instances_devices_assembly_index ON
    assembly_instances_devices (assembly_id, assembly_instance_id);
CREATE INDEX assembly_instances_devices_device_index ON
    assembly_instances_devices (device_type_id, device_id);
CREATE INDEX assembly_instances_devices_assembly_device_index ON
    assembly_instances_devices (population_id, assembly_id, device_type_id);
CREATE INDEX assembly_instances_devices_device_id ON
    assembly_instances_devices (device_id);

CREATE TABLE transducer_types (
    transducer_type_id  serial PRIMARY KEY,
    transducer_type     citext UNIQUE NOT NULL -- e.g. 'electrode', 'microphone'
);
CREATE INDEX transducer_type_name_index ON
    transducer_types (transducer_type text_pattern_ops);

-- The pin on which a 'transduced' signal appears, e.g. a dimension of a gyro
CREATE TABLE transducers (
    pin_id          integer REFERENCES device_type_pins,
    transducer_type_id integer REFERENCES transducer_types,
    PRIMARY KEY (pin_id, transducer_type_id)
);
CREATE INDEX transducers_transducer_type_id_index ON transducers (transducer_type_id);

CREATE TABLE signal_acquirers (
    acquirer_id        serial PRIMARY KEY,
    device_id          integer NOT NULL,
    device_type_id     integer NOT NULL,
    pin_id             integer NOT NULL,
    UNIQUE (device_id, pin_id),
    FOREIGN KEY (device_id, device_type_id) REFERENCES
        devices (device_id, device_type_id),
    FOREIGN KEY (pin_id, device_type_id) REFERENCES
        device_type_pins (pin_id, device_type_id)
);
CREATE INDEX signal_acquirers_device_type_id_index ON
    signal_acquirers (device_type_id);
CREATE INDEX signal_acquirers_pin_id_index ON
    signal_acquirers (pin_id);

CREATE TABLE spatial_groups (
    spatial_group_id   serial PRIMARY KEY,
    spatial_group_name citext UNIQUE NOT NULL
);
CREATE INDEX spatial_groups_name_index ON spatial_groups (spatial_group_name text_pattern_ops);

CREATE TABLE spatial_points (
    point_id           serial PRIMARY KEY,
    spatial_group_id   integer REFERENCES spatial_groups NOT NULL,
    point_name         citext NOT NULL,
    point_x            double precision NOT NULL, -- Microns
    point_y            double precision NOT NULL,
    point_z            double precision NOT NULL,
    UNIQUE (spatial_group_id, point_name)
);
CREATE INDEX spatial_points_point_name_index ON spatial_points (point_name);
CREATE INDEX spatial_points_point_name_ops_index ON spatial_points (point_name text_pattern_ops);

CREATE TABLE assembly_instances_devices_points (
    assembly_instance_id integer,
    device_id            integer,
    point_id             integer REFERENCES spatial_points NOT NULL,
    PRIMARY KEY (assembly_instance_id, device_id),
    FOREIGN KEY (assembly_instance_id, device_id) REFERENCES
        assembly_instances_devices (assembly_instance_id, device_id)
);
CREATE INDEX assembly_instances_devices_points_point_index ON
    assembly_instances_devices_points (point_id);

-- Recording contexts


CREATE TABLE contexts (
    context_id          serial PRIMARY KEY
);

CREATE TABLE contexts_birds (
    context_id          integer REFERENCES contexts PRIMARY KEY,
    bird_id             integer REFERENCES birds UNIQUE
);

CREATE TABLE test_context (
    context_id         integer REFERENCES contexts PRIMARY KEY,
    context_parameters jsonb NOT NULL
);
CREATE INDEX test_context_param_index ON test_context USING gin (context_parameters);

CREATE FUNCTION unique_contexts_birds()
RETURNS trigger AS $unique_contexts_birds$
    DECLARE
        test_context_id integer;
    BEGIN
        SELECT test_context.context_id INTO test_context_id
            FROM test_context
            WHERE test_context.context_id = NEW.context_id;
        IF FOUND THEN
           RAISE EXCEPTION
               'context (%) is already in test_context', test_context_id;
        END IF;
        RETURN NEW;
    END;
$unique_contexts_birds$ LANGUAGE plpgsql;

CREATE TRIGGER unique_contexts_birds
    BEFORE INSERT OR UPDATE ON contexts_birds
    FOR EACH ROW EXECUTE PROCEDURE unique_contexts_birds();


CREATE FUNCTION unique_contexts_test()
    RETURNS trigger AS $unique_contexts_test$
    DECLARE
        birds_context_id integer;
    BEGIN
        SELECT contexts_birds.context_id INTO birds_context_id
            FROM context_birds
            WHERE contexts_birds.context_id = NEW.context_id;
        IF FOUND THEN
            RAISE EXCEPTION
                'context (%) is already in birds_context', birds_context_id;
        END IF;
        RETURN NEW;
   END;
$unique_contexts_test$ LANGUAGE plpgsql;

CREATE TRIGGER unique_contexts_test
   BEFORE INSERT OR UPDATE ON test_context
   FOR EACH ROW EXECUTE PROCEDURE unique_contexts_test();


-- Recordings


CREATE TABLE recordings (
    recording_id         serial PRIMARY KEY,
    recording_time_range tstzrange NOT NULL,
    context_id           integer REFERENCES contexts NOT NULL,
    acquirer_id          integer REFERENCES signal_acquirers NOT NULL,
    recording_parameters jsonb,
    UNIQUE (recording_time_range, acquirer_id),
    UNIQUE (recording_id, context_id)
);
CREATE INDEX recording_time_range_index ON recordings USING gist (recording_time_range);
CREATE INDEX recording_context_index ON recordings (context_id);
CREATE INDEX recording_acquirer_id ON recordings (acquirer_id);
CREATE INDEX recordings_parameters_index ON recordings USING gin (recording_parameters);

CREATE TABLE contexts_assembly_instances (
    context_id           integer REFERENCES contexts,
    assembly_instance_id integer REFERENCES assembly_instances,
    PRIMARY KEY (context_id, assembly_instance_id)
);
CREATE INDEX contexts_assembly_instances_instance_idx ON
    contexts_assembly_instances (assembly_instance_id);

CREATE TABLE notes (
    note_id             serial PRIMARY KEY,
    recording_note      jsonb NOT NULL,
    note_name           citext UNIQUE
);
CREATE INDEX note_note_name_ops_index ON notes (note_name text_pattern_ops);
CREATE INDEX notes_recording_note_index ON notes USING gin (recording_note);

CREATE TABLE recordings_notes (
    recording_id        integer REFERENCES recordings ON DELETE RESTRICT,
    note_id             integer REFERENCES notes,
    PRIMARY KEY (recording_id, note_id)
);
CREATE INDEX recordings_notes_note_index ON recordings_notes (note_id);

CREATE OR REPLACE FUNCTION note_is_orphan(my_note_id integer)
    RETURNS boolean AS $note_is_orphan$
    BEGIN
        RETURN NOT EXISTS (SELECT 1
        FROM recordings_notes rn
        WHERE rn.note_id = my_note_id
        ) AND EXISTS (SELECT 1
        FROM notes n
        WHERE n.note_id = my_note_id
        );
    END;
$note_is_orphan$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION notes_no_orphan_trg()
    RETURNS trigger AS $notes_no_orphan_trg$
    BEGIN
        IF (note_is_orphan(NEW.note_id)) THEN
            RAISE EXCEPTION
                'Note with note_id % is not referenced in recordings_notes',
                NEW.note_id;
        END IF;
        RETURN NEW;
    END;
$notes_no_orphan_trg$ LANGUAGE plpgsql;

CREATE CONSTRAINT TRIGGER notes_no_orphan_notes_trg
    AFTER INSERT OR UPDATE ON notes
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE PROCEDURE notes_no_orphan_trg();

CREATE OR REPLACE FUNCTION recordings_notes_no_orphan_trg()
    RETURNS trigger AS $recordings_notes_no_orphan_trg$
    DECLARE
        old_note_id integer;
    BEGIN
        SELECT note_id INTO old_note_id
            FROM notes
            WHERE notes.note_id = OLD.note_id;
        IF (FOUND AND note_is_orphan(old_note_id)) THEN
            RAISE EXCEPTION
                'Note with note_id % is not referenced in recordings_notes',
                old_note_id;
        END IF;
        RETURN OLD;
    END;
    $recordings_notes_no_orphan_trg$ LANGUAGE plpgsql;

CREATE CONSTRAINT TRIGGER recordings_notes_no_orphan_notes_trg
    AFTER UPDATE OR DELETE ON recordings_notes
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE PROCEDURE recordings_notes_no_orphan_trg();

CREATE TABLE units (
    unit_id             serial PRIMARY KEY,
    unit_name           citext UNIQUE NOT NULL -- e.g. 'microvolts'
);
CREATE INDEX units_unit_name_ops_index ON units (unit_name text_pattern_ops);

CREATE TABLE recordings_timeseries  (
    recording_id        integer REFERENCES recordings ON DELETE CASCADE PRIMARY KEY,
    unit_id             integer REFERENCES units
);
CREATE INDEX recordings_timeseries_unit_index ON recordings_timeseries (unit_id);

CREATE TABLE timeseries_neural (
    recording_id        integer REFERENCES recordings_timeseries ON DELETE CASCADE PRIMARY KEY,
    channel_number      integer CHECK (channel_number >= 0) NOT NULL,
    electrode_depth     double precision, -- microns
    lateral_position    double precision
);

CREATE TABLE rawarraydata(
    recording_id         integer REFERENCES recordings ON DELETE RESTRICT PRIMARY KEY,
    sampling_frequency   double precision CHECK (sampling_frequency > 0) NOT NULL,
    data_values          double precision[] NOT NULL
);

CREATE OR REPLACE FUNCTION rawarraydata_duration_trg()
RETURNS trigger AS $rawarraydata_duration_trg$
    DECLARE
        recording_duration double precision;
        sampling_period double precision;
        array_duration double precision;
    BEGIN
        -- Calculate duration of recording
        SELECT extract(
                epoch FROM
                upper(recordings.recording_time_range) -
                lower(recordings.recording_time_range)
            )
            INTO STRICT recording_duration
            FROM recordings
            WHERE recordings.recording_id = NEW.recording_id;
        -- Calculate sampling period
        sampling_period := (1 / NEW.sampling_frequency);
        -- Calculate duration of array data
        array_duration := sampling_period * COALESCE(array_length(NEW.data_values, 1), 0);
        IF (abs(recording_duration - array_duration) > sampling_period) THEN
            RAISE EXCEPTION
                'recording duration (%) does not match array duration (%)',
                recording_duration, array_duration;
        END IF;
        RETURN NEW;
    END;
$rawarraydata_duration_trg$ LANGUAGE plpgsql;

CREATE TRIGGER rawarraydata_duration_trg
    BEFORE INSERT OR UPDATE ON rawarraydata
    FOR EACH ROW EXECUTE PROCEDURE rawarraydata_duration_trg();


CREATE TABLE rawdata(
    recording_id        integer REFERENCES recordings ON DELETE RESTRICT NOT NULL,
    sample_time         timestamp NOT NULL,
    data_value          double precision NOT NULL,
    PRIMARY KEY (recording_id, sample_time)
);

CREATE FUNCTION unique_rawarraydata_recording_trg()
    RETURNS trigger AS $unique_rawarraydata_recording_trg$
    DECLARE
        rawdata_recording integer;
    BEGIN
        -- Look up the recording id in rawdata (rawdata_recording)
        SELECT DISTINCT rawdata.recording_id INTO rawdata_recording
            FROM rawdata
            WHERE rawdata.recording_id = NEW.recording_id;
        -- Enforce unique data source
        IF FOUND THEN
            RAISE EXCEPTION
                'recording_id (%) already in rawdata',
                rawdata_recording;
        END IF;
        RETURN NEW;
    END;
$unique_rawarraydata_recording_trg$ LANGUAGE plpgsql;

CREATE TRIGGER unique_rawarraydata_recording_trg
    BEFORE INSERT OR UPDATE ON rawarraydata
    FOR EACH ROW EXECUTE PROCEDURE unique_rawarraydata_recording_trg();

CREATE FUNCTION unique_rawdata_recording_trg()
    RETURNS trigger AS $unique_rawdata_recording_trg$
    DECLARE
        rawarraydata_recording integer;
    BEGIN
        SELECT rawarraydata.recording_id INTO rawarraydata_recording
            FROM rawarraydata
            WHERE rawarraydata.recording_id = NEW.recording_id;
        IF FOUND THEN
           RAISE EXCEPTION
                 'recording_id (%) already in rawarraydata',
                 rawarraydata_recording;
        END IF;
        RETURN NEW;
    END;
$unique_rawdata_recording_trg$ LANGUAGE plpgsql;

CREATE TRIGGER rawdata_recording_trg
    BEFORE INSERT OR UPDATE ON rawdata
    FOR EACH ROW EXECUTE PROCEDURE unique_rawdata_recording_trg();

CREATE TABLE exclusions (
  exclusion_id         serial PRIMARY KEY,
  exclusion_time_range tstzrange NOT NULL
);
CREATE INDEX exclusion_time_range_index ON exclusions USING gist (exclusion_time_range);

CREATE TABLE recordings_exclusions (
  recording_id          integer REFERENCES recordings,
  exclusion_id          integer REFERENCES exclusions,
  PRIMARY KEY (recording_id, exclusion_id)
);
CREATE INDEX recordings_exclusions_excl_idx ON recordings_exclusions (exclusion_id);

CREATE OR REPLACE FUNCTION exclusion_is_orphan(my_exclusion_id integer)
   RETURNS boolean AS $exclusion_is_orphan$
   BEGIN
      RETURN NOT EXISTS (SELECT 1
          FROM recordings_exclusions re
          WHERE re.exclusion_id = my_exclusion_id
      );
   END;
   $exclusion_is_orphan$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION exclusions_no_orphan_trg()
    RETURNS trigger AS $exclusions_no_orphan_trg$
    BEGIN
        IF (exclusion_is_orphan(NEW.exclusion_id)) THEN
            RAISE EXCEPTION
            'Exclusion with exclusion_id % is not referenced in recordings_exclusions',
            NEW.exclusion_id;
        END IF;
        RETURN NEW;
    END;
    $exclusions_no_orphan_trg$ LANGUAGE plpgsql;

CREATE CONSTRAINT TRIGGER exclusions_no_orphan_exclusions_trg
    AFTER INSERT OR UPDATE ON exclusions
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE PROCEDURE exclusions_no_orphan_trg();

CREATE OR REPLACE FUNCTION recordings_exclusions_no_orphan_trg()
   RETURNS trigger AS $recordings_exclusions_no_orphan_trg$
   DECLARE
       old_exclusion_id integer;
   BEGIN
       SELECT exclusion_id INTO old_exclusion_id
          FROM exclusions
          WHERE exclusions.exclusion_id = OLD.exclusion_id;
       IF (FOUND AND exclusion_is_orphan(old_exclusion_id)) THEN
          RAISE EXCEPTION
              'Exclusion with exclusion_id % is not referenced in recordings_exclusions',
              old_exclusion_id;
       END IF;
       RETURN OLD;
   END;
$recordings_exclusions_no_orphan_trg$ LANGUAGE plpgsql;

CREATE CONSTRAINT TRIGGER recordings_exclusions_no_orphan_labels_trg
    AFTER UPDATE OR DELETE ON recordings_exclusions
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE PROCEDURE recordings_exclusions_no_orphan_trg();

CREATE OR REPLACE FUNCTION exclusion_off_recording_trg()
   RETURNS trigger AS
   $func$
        BEGIN
        IF NOT (
            (
              SELECT recording_time_range
              FROM recordings
              WHERE recordings.recording_id = NEW.recording_id
            )
            &&
            (
                SELECT exclusion_time_range
                FROM exclusions
                WHERE exclusions.exclusion_id = NEW.exclusion_id
            )
        )
        THEN
        RAISE EXCEPTION 'Exclusion and recording time ranges do not overlap';
        END IF;
        RETURN NEW;
        END;
   $func$ LANGUAGE plpgsql;

CREATE TRIGGER exclusion_off_recording_trg
BEFORE INSERT OR UPDATE ON recordings_exclusions
FOR EACH ROW EXECUTE PROCEDURE exclusion_off_recording_trg();


CREATE TABLE hosts (
   host_id             serial PRIMARY KEY,
   host_name           citext UNIQUE NOT NULL
);
CREATE INDEX hosts_name_ops_index ON hosts (host_name text_pattern_ops);

CREATE TABLE formats (
   format_id          serial PRIMARY KEY,
   format_name        citext UNIQUE NOT NULL
);
CREATE INDEX formats_name_ops_index ON formats (format_name text_pattern_ops);

CREATE TABLE paths (
   path_id           serial PRIMARY KEY,
   host_id           integer REFERENCES hosts NOT NULL,
   path_name         text NOT NULL,
   UNIQUE (host_id, path_name)
);
CREATE INDEX paths_name_index ON paths (path_name);
CREATE INDEX paths_name_ops_index ON paths (path_name text_pattern_ops);
CREATE INDEX paths_host_idx ON paths (host_id);

CREATE TABLE files (
    file_id             serial PRIMARY KEY,
    file_name           text NOT NULL,
    path_id             integer REFERENCES paths NOT NULL,
    format_id           integer REFERENCES formats NOT NULL,
    UNIQUE (file_name, path_id)
);
CREATE INDEX files_file_name_ops_idx ON files (file_name text_pattern_ops);
CREATE INDEX files_format_idx ON files (format_id);
CREATE INDEX files_path_idx ON files (path_id);

CREATE TABLE files_recordings (
    file_id             integer REFERENCES files,
    recording_id        integer REFERENCES recordings,
    rec_position        integer,
    PRIMARY KEY (file_id, recording_id)
);
CREATE INDEX files_recordings_rec_idx ON files_recordings (recording_id);

CREATE OR REPLACE FUNCTION file_is_orphan(my_file_id integer)
    RETURNS boolean AS $file_is_orphan$
    BEGIN
        RETURN NOT EXISTS (SELECT 1
        FROM files_recordings fr
        WHERE fr.file_id = my_file_id
        );
    END;
$file_is_orphan$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION files_no_orphan_trg()
    RETURNS trigger AS $files_no_orphan_trg$
    BEGIN
        IF (file_is_orphan(NEW.file_id)) THEN
            RAISE EXCEPTION
                'File with file_id % is not referenced in files_recordings',
                NEW.file_id;
        END IF;
        RETURN NEW;
    END;
$files_no_orphan_trg$ LANGUAGE plpgsql;

CREATE CONSTRAINT TRIGGER files_no_orphan_files_trg
    AFTER INSERT OR UPDATE ON files
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE PROCEDURE files_no_orphan_trg();

CREATE OR REPLACE FUNCTION files_recordings_no_orphan_trg()
    RETURNS trigger AS $files_recordings_no_orphan_trg$
    DECLARE
        old_file_id integer;
    BEGIN
        SELECT file_id INTO old_file_id
            FROM files
            WHERE files.file_id = OLD.file_id;
        IF (FOUND AND file_is_orphan(old_file_id)) THEN
            RAISE EXCEPTION
                'File with file_id % is not referenced in files_recordings',
                old_file_id;
        END IF;
        RETURN OLD;
    END;
$files_recordings_no_orphan_trg$ LANGUAGE plpgsql;

CREATE CONSTRAINT TRIGGER files_recordings_no_orphan_files_trg
    AFTER UPDATE OR DELETE ON files_recordings
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE PROCEDURE files_recordings_no_orphan_trg();

-- Events

CREATE TABLE event_types (
    type_id             serial PRIMARY KEY,
    type_name           citext UNIQUE NOT NULL, -- e.g. 'spike', 'syllable'
    type_description    text
);
CREATE INDEX event_types_type_name_ops_index ON
    event_types (type_name text_pattern_ops);

CREATE TABLE event_detector_types (
    detector_type_id    serial PRIMARY KEY,
    detector_name       citext UNIQUE NOT NULL -- algorithm
);
CREATE INDEX event_detectors_types_detector_name_index ON
    event_detector_types (detector_name text_pattern_ops);

CREATE TABLE event_detectors (
    detector_id         serial PRIMARY KEY,
    detector_type_id    integer REFERENCES event_detector_types NOT NULL,
    detector_software   text NOT NULL, -- function name
    detector_version    citext, -- sha1
    detector_parameters jsonb,
    UNIQUE (detector_type_id, detector_software, detector_version, detector_parameters)
);
CREATE INDEX event_detectors_detector_software_index ON event_detectors (
       detector_software text_pattern_ops
);
CREATE INDEX event_detector_parameter_index ON event_detectors USING gin (
       detector_parameters
);

-- Creating events requires the use of a transaction
-- First, create entries in recordings_events, then add the events
CREATE TABLE events (
    event_id            serial PRIMARY KEY,
    type_id             integer REFERENCES event_types NOT NULL,
    detector_id         integer REFERENCES event_detectors NOT NULL,
    event_time_range    tstzrange NOT NULL,
    passed_review       boolean NOT NULL DEFAULT false,
    context_id          integer REFERENCES contexts NOT NULL,
    UNIQUE (event_id, context_id)
);
CREATE INDEX events_type_index ON events (type_id);
CREATE INDEX events_detector_index ON events (detector_id);
CREATE INDEX events_context_index ON events (context_id);
CREATE INDEX event_time_index ON events USING gist (event_time_range);
CREATE INDEX event_timecontext_index ON events USING gist (
       context_id, event_time_range
);
CREATE INDEX event_partial_unique_index ON events USING gist (
       type_id, detector_id, event_time_range, context_id
);

CREATE TABLE events_amplitudes (
   event_id             integer REFERENCES events PRIMARY KEY,
   unit_id              integer REFERENCES units NOT NULL,
   amplitude            double precision NOT NULL
);

-- the above index requires btree_gist extension

CREATE TABLE recordings_events (
    recording_id        integer,
    event_id            integer,
    context_id          integer NOT NULL,
    PRIMARY KEY (recording_id, event_id),
    FOREIGN KEY (recording_id, context_id) REFERENCES
        recordings (recording_id, context_id) ON DELETE RESTRICT,
    FOREIGN KEY (event_id, context_id) REFERENCES events (event_id, context_id)
);
CREATE INDEX recordings_events_event_index ON recordings_events (event_id);
CREATE INDEX recordings_events_contex_index ON recordings_events (context_id);

CREATE OR REPLACE FUNCTION event_is_orphan(my_event_id integer)
    RETURNS boolean AS $event_is_orphan$
    BEGIN
        RETURN NOT EXISTS (SELECT 1
        FROM recordings_events re
        WHERE re.event_id = my_event_id
        );
    END;
$event_is_orphan$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION events_no_orphan_trg()
    RETURNS trigger AS $events_no_orphan_trg$
    BEGIN
        IF (event_is_orphan(NEW.event_id)) THEN
            RAISE EXCEPTION
                'Event with event_id % is not referenced in recordings_events',
                NEW.event_id;
        END IF;
         RETURN NEW;
    END;
$events_no_orphan_trg$ LANGUAGE plpgsql;

CREATE CONSTRAINT TRIGGER events_no_orphan_events_trg
AFTER INSERT OR UPDATE ON events
DEFERRABLE INITIALLY DEFERRED
FOR EACH ROW EXECUTE PROCEDURE events_no_orphan_trg();

CREATE OR REPLACE FUNCTION recordings_events_no_orphan_trg()
    RETURNS trigger AS $recordings_events_no_orphan_trg$
    DECLARE
        old_event_id integer;
    BEGIN
        SELECT event_id INTO old_event_id
            FROM events
            WHERE events.event_id = OLD.event_id;
        IF (FOUND AND event_is_orphan(old_event_id)) THEN
            RAISE EXCEPTION
                'Event with event_id % is not referenced in recordings_events',
                old_event_id;
        END IF;
        RETURN OLD;
    END;
    $recordings_events_no_orphan_trg$ LANGUAGE plpgsql;

CREATE CONSTRAINT TRIGGER recordings_events_no_orphan_events_trg
    AFTER UPDATE OR DELETE ON recordings_events
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE PROCEDURE recordings_events_no_orphan_trg();

CREATE OR REPLACE FUNCTION event_off_recording_trg()
    RETURNS trigger AS
$func$
DECLARE
    existing_recording_time_range tstzrange;
    existing_event_time_range tstzrange;
BEGIN
    SELECT recording_time_range INTO STRICT existing_recording_time_range
    FROM recordings
    WHERE recordings.recording_id = NEW.recording_id;
    SELECT event_time_range INTO STRICT existing_event_time_range
    FROM events
    WHERE events.event_id = new.event_id;
    IF NOT existing_recording_time_range && existing_event_time_range
    THEN
      RAISE EXCEPTION
            'Recording time range % and event time range % not overlap',
            existing_recording_time_range, existing_event_time_range;
    END IF;
    RETURN NEW;
END;
$func$ LANGUAGE plpgsql;

CREATE TRIGGER event_off_recording_trg
    BEFORE INSERT OR UPDATE ON recordings_events
    FOR EACH ROW EXECUTE PROCEDURE event_off_recording_trg();

CREATE TABLE labels (
    label_id            serial PRIMARY KEY,
    label_name          text NOT NULL, -- e.g. 'A' or 'B' for syllable;
                                       --      'SingleUnit001' for spike
    label_description   text,
    context_id          integer REFERENCES contexts,
    UNIQUE (label_name, context_id),
    UNIQUE (context_id, label_id)
);
CREATE INDEX labels_label_name_ops_index ON labels (label_name text_pattern_ops);

CREATE TABLE events_labels (
    event_id            integer,
    label_id            integer,
    context_id          integer NOT NULL,
    PRIMARY KEY (event_id, label_id),
    FOREIGN KEY (event_id, context_id) REFERENCES events (event_id, context_id),
    FOREIGN KEY (context_id, label_id) REFERENCES labels (context_id, label_id)
);
CREATE INDEX events_labels_labels_index ON events_labels (label_id);
CREATE INDEX events_labels_context_index ON events_labels (context_id);

CREATE OR REPLACE FUNCTION label_is_orphan(my_label_id integer)
    RETURNS boolean AS $label_is_orphan$
    BEGIN
        RETURN NOT EXISTS (SELECT 1
        FROM events_labels el
        WHERE el.label_id = my_label_id
        );
    END;
$label_is_orphan$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION labels_no_orphan_trg()
    RETURNS trigger AS $labels_no_orphan_trg$
    BEGIN
        IF (label_is_orphan(NEW.label_id)) THEN
            RAISE EXCEPTION
                'Label with label_id % is not referenced in events_labels',
                NEW.label_id;
        END IF;
         RETURN NEW;
    END;
$labels_no_orphan_trg$ LANGUAGE plpgsql;

CREATE CONSTRAINT TRIGGER labels_no_orphan_labels_trg
AFTER INSERT OR UPDATE ON labels
DEFERRABLE INITIALLY DEFERRED
FOR EACH ROW EXECUTE PROCEDURE labels_no_orphan_trg();

CREATE OR REPLACE FUNCTION events_labels_no_orphan_trg()
    RETURNS trigger AS $events_labels_no_orphan_trg$
    DECLARE
        old_label_id integer;
    BEGIN
        SELECT label_id INTO old_label_id
            FROM labels
            WHERE labels.label_id = OLD.label_id;
        IF (FOUND AND label_is_orphan(old_label_id)) THEN
            RAISE EXCEPTION
                'Label with label_id % is not referenced in events_labels',
                old_label_id;
        END IF;
        RETURN OLD;
    END;
    $events_labels_no_orphan_trg$ LANGUAGE plpgsql;

CREATE CONSTRAINT TRIGGER events_labels_no_orphan_labels_trg
    AFTER UPDATE OR DELETE ON events_labels
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE PROCEDURE events_labels_no_orphan_trg();

CREATE OR REPLACE FUNCTION event_is_duplicate(my_event_id integer)
    RETURNS boolean AS $func$
    BEGIN
        RETURN EXISTS (
        SELECT 1
        FROM events e1
        INNER JOIN events e2 USING (
              type_id, detector_id, event_time_range, context_id
        )
        INNER JOIN recordings_events re1 ON (re1.event_id = e1.event_id)
        INNER JOIN recordings_events re2 ON (
              re2.event_id = e2.event_id AND re2.recording_id = re1.recording_id
        )
        LEFT JOIN events_labels el1 ON (el1.event_id = e1.event_id)
        INNER JOIN events_labels el2 ON (
              el2.event_id = e2.event_id
              AND el2.label_id IS NOT DISTINCT FROM el1.label_id
        )
        WHERE e1.event_id = my_event_id
        AND e2.event_id <> e1.event_id
        );
    END;
    $func$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION new_events_no_dups()
    RETURNS trigger AS $events_no_dups_trg$
    BEGIN
      IF (event_is_duplicate(NEW.event_id)) THEN
          RAISE EXCEPTION
             'event with event_id % is redundant',
             NEW.event_id;
      END IF;
      RETURN NEW;
    END;
$events_no_dups_trg$ LANGUAGE plpgsql;

CREATE CONSTRAINT TRIGGER events_no_dups_trg
   AFTER INSERT OR UPDATE ON events
   DEFERRABLE INITIALLY DEFERRED
   FOR EACH ROW EXECUTE PROCEDURE new_events_no_dups();

CREATE CONSTRAINT TRIGGER recordings_events_no_dups_trg
   AFTER INSERT OR UPDATE ON recordings_events
   DEFERRABLE INITIALLY DEFERRED
   FOR EACH ROW EXECUTE PROCEDURE new_events_no_dups();

CREATE OR REPLACE FUNCTION old_events_no_dups()
    RETURNS trigger AS $events_no_dups_trg$
    BEGIN
    IF (event_is_duplicate(OLD.event_id)) THEN
       RAISE EXCEPTION
       'event with event_id % is redundant',
       OLD.event_id;
    END IF;
    RETURN OLD;
    END;
$events_no_dups_trg$ LANGUAGE plpgsql;

CREATE CONSTRAINT TRIGGER events_labels_no_dups_trg
    AFTER UPDATE OR DELETE ON events_labels
    DEFERRABLE INITIALLY DEFERRED
    FOR EACH ROW EXECUTE PROCEDURE old_events_no_dups();

CREATE TABLE derived_events (
    derived_event_id                integer PRIMARY KEY,
    original_event_id               integer NOT NULL,
    context_id                      integer NOT NULL,
    FOREIGN KEY (derived_event_id, context_id) REFERENCES events (event_id, context_id),
    FOREIGN KEY (original_event_id, context_id) REFERENCES events (event_id, context_id)
);
CREATE INDEX derived_events_original_event_index ON
       derived_events (original_event_id);
CREATE INDEX derived_events_derived_context_index ON
       derived_events (derived_event_id, context_id);
CREATE INDEX derived_events_original_context_index ON
       derived_events (original_event_id, context_id);

CREATE TABLE label_support (
   label_id           integer REFERENCES labels NOT NULL,
   label_time_range   tstzrange NOT NULL,
   PRIMARY KEY (label_id, label_time_range)
);
CREATE INDEX label_time_range_index ON label_support USING gist (label_time_range);

-- Views

CREATE VIEW birds_with_info AS (
    WITH mothers AS (
        SELECT child_id AS bird_id, parent_id AS mother_id
        FROM birds_parentage WHERE relationship='mother'
    ), fathers AS (
        SELECT child_id AS bird_id, parent_id AS father_id
        FROM birds_parentage WHERE relationship='father'
    )
    SELECT *
        FROM birds LEFT OUTER JOIN birds_separation USING (bird_id)
                   LEFT OUTER JOIN surgeries USING (bird_id)
                   LEFT OUTER JOIN mothers USING (bird_id)
                   LEFT OUTER JOIN fathers USING (bird_id)
);
-- Has all the columns from birds, birds_separation, surgeries. Also has
-- columns called mother_id and father_id with the bird_id of each bird's
-- mother and father.

CREATE OR REPLACE VIEW files_with_info AS (
   SELECT *
   FROM files INNER JOIN paths USING (path_id)
              INNER JOIN hosts USING (host_id)
              INNER JOIN formats USING (format_id)
);

CREATE OR REPLACE VIEW recordings_with_info AS (
    SELECT recording_id,
           recording_time_range,
           context_id,
           acquirer_id,
           recording_parameters,
           jsonb_agg(recording_note) FILTER (
                                     WHERE recording_note IS NOT NULL
                                     ) AS agg_note,
           unit_name,
           channel_number,
           electrode_depth,
           rec_position,
           file_id,
           file_name,
           path_name,
           host_name,
           format_name
        FROM recordings
        LEFT OUTER JOIN recordings_timeseries USING (recording_id)
        LEFT OUTER JOIN units USING (unit_id)
        LEFT OUTER JOIN timeseries_neural USING (recording_id)
        LEFT OUTER JOIN files_recordings USING (recording_id)
        LEFT OUTER JOIN files USING (file_id)
        LEFT OUTER JOIN paths USING (path_id)
        LEFT OUTER JOIN hosts USING (host_id)
        LEFT OUTER JOIN formats USING (format_id)
        LEFT OUTER JOIN recordings_notes USING (recording_id)
        LEFT OUTER JOIN notes USING (note_id)
        GROUP BY recording_id,
        recording_time_range,
        context_id,
        acquirer_id,
        recording_parameters,
        unit_name,
        channel_number,
        electrode_depth,
        rec_position,
        file_id,
        file_name,
        path_name,
        host_name,
        format_name
);

CREATE OR REPLACE VIEW events_with_info AS (
    SELECT event_id,
          context_id,
          event_time_range,
          passed_review,
          type_id,
          type_name,
          type_description,
          detector_type_id,
          detector_name,
          detector_id,
          detector_software,
          detector_parameters,
          detector_version,
          array_agg(recording_id) AS recording_ids,
          array_agg(label_id) AS label_ids,
          array_agg(label_name) AS label_names
      FROM events
      INNER JOIN event_types USING (type_id)
      INNER JOIN event_detectors USING (detector_id)
      INNER JOIN event_detector_types USING (detector_type_id)
      INNER JOIN recordings_events USING (event_id, context_id)
      LEFT OUTER JOIN events_amplitudes USING (event_id)
      LEFT OUTER JOIN events_labels USING (event_id, context_id)
      LEFT OUTER JOIN labels USING (label_id, context_id)
      GROUP BY event_id,
              context_id,
              event_time_range,
              passed_review,
              type_id,
              type_name,
              type_description,
              detector_type_id,
              detector_name,
              detector_id,
              detector_software,
              detector_parameters,
              detector_version
);
-- Has all the columns from events, event_types, event_detectors, and
-- recordings_with_info. It also has a new column called 'label_names' which
-- is an array of label_names associated with each event.

CREATE OR REPLACE VIEW subcomponent_population AS (
    SELECT apc.assembly_id,
           ph.parent_population_id,
           dtp.device_type_name AS parent_device,
           scp.subcomponent_name,
           apc.population_id AS child_population_id,
           dtc.device_type_name AS child_device
    FROM population_hierarchy ph
        INNER JOIN subcomponents scp USING (subcomponent_id)
        INNER JOIN device_types dtp ON (ph.parent_device_type_id = dtp.device_type_id)
        INNER JOIN assembly_population apc USING (population_id)
        INNER JOIN device_types dtc ON (apc.device_type_id = dtc.device_type_id)
);

CREATE OR REPLACE VIEW pop_pins_by_device AS (
    SELECT *
    FROM assembly_population ap
         INNER JOIN device_type_pins dtp USING (device_type_id)
         INNER JOIN assembly_instances ai USING (assembly_id)
         INNER JOIN assembly_instances_devices aid USING (
               assembly_instance_id, population_id, assembly_id, device_type_id)
         INNER JOIN devices d USING (device_id, device_type_id)
);

CREATE OR REPLACE VIEW signal_acquirers_with_info AS (
    SELECT *
    FROM signal_acquirers sa
         NATURAL JOIN assembly_instances_devices aid
         NATURAL JOIN assembly_instances ai
);

CREATE OR REPLACE VIEW transducers_with_info AS (
    SELECT *
    FROM transducers td
         NATURAL JOIN transducer_types tt
         NATURAL JOIN device_type_pins dtp
         NATURAL JOIN device_types dt
);

CREATE OR REPLACE VIEW connected_pins AS (
    WITH RECURSIVE pin_net AS (
        SELECT
            a.assembly_id,
            pop.population_id AS seed_population_id,
            pin.pin_id AS seed_pin_id,
            pop.population_id AS population_id,
            pin.pin_id AS pin_id,
            0 AS connection_distance,
            ARRAY[CAST(ROW(pop.population_id, pin.pin_id) AS pop_pin)] AS pin_path,
            false AS is_cycle
        FROM assemblies a
            INNER JOIN assembly_population pop USING (assembly_id)
            INNER JOIN device_type_pins pin USING (device_type_id)
        UNION ALL
        SELECT
            pin_net.assembly_id,
            pin_net.seed_population_id,
            pin_net.seed_pin_id,
            pc.to_population_id AS population_id,
            pc.to_pin_id AS pin_id,
            pin_net.connection_distance + 1 AS connection_distance,
            pin_net.pin_path || CAST(ROW(pc.to_population_id, pc.to_pin_id) AS pop_pin) AS pin_path,
            CAST(ROW(pc.to_population_id, pc.to_pin_id) AS pop_pin) = ANY(pin_net.pin_path) AS is_cycle
         FROM pin_connections pc, pin_net
         WHERE pc.assembly_id = pin_net.assembly_id
             AND pc.from_population_id = pin_net.population_id
             AND pc.from_pin_id = pin_net.pin_id
             AND NOT pin_net.is_cycle
    )
    SELECT assembly_id,
           seed_population_id,
           seed_pin_id,
           population_id,
           pin_id,
           connection_distance,
           pin_path
    FROM pin_net
    WHERE NOT is_cycle
);

CREATE OR REPLACE VIEW connected_transducers_acquirers AS (
   SELECT transducer_type_id, dtp.pin_id, aid2.assembly_instance_id, acquirer_id
   FROM transducer_types tt
      NATURAL INNER JOIN transducers t
      NATURAL INNER JOIN device_type_pins dtp
      NATURAL INNER JOIN assembly_population ap
      INNER JOIN connected_pins cp ON (cp.seed_population_id = ap.population_id
                                       AND cp.seed_pin_id = dtp.pin_id)
      INNER JOIN assembly_instances_devices aid2 ON (aid2.population_id = cp.population_id)
      INNER JOIN signal_acquirers sa ON (sa.device_id = aid2.device_id
                                         AND sa.pin_id = cp.pin_id)
);

CREATE OR REPLACE VIEW recordings_with_spatial AS (
    SELECT r.*,
           aid.device_type_id, pi.pin_id, aid.device_id,
           ai.assembly_id, pi.population_id,
           aidsa.assembly_instance_id, ai.assembly_instance_name,
           spatial_group_id, aidp.point_id, point_name,
           point_x, point_y, point_z
    FROM recordings r
         INNER JOIN contexts_assembly_instances cai USING (context_id)
         INNER JOIN signal_acquirers sa USING (acquirer_id)
         INNER JOIN assembly_instances_devices aidsa USING (device_id, assembly_instance_id)
         INNER JOIN assembly_instances ai USING (assembly_instance_id)
         INNER JOIN connected_pins pi ON (
               pi.assembly_id = ai.assembly_id
               AND pi.seed_population_id = aidsa.population_id
               AND pi.seed_pin_id = sa.pin_id
         )
         INNER JOIN assembly_instances_devices aid ON (
               aid.assembly_instance_id = cai.assembly_instance_id
               AND aid.population_id = pi.population_id
         )
         INNER JOIN assembly_instances_devices_points aidp ON (
               aidp.assembly_instance_id = cai.assembly_instance_id
               AND aidp.device_id = aid.device_id
         )
         INNER JOIN spatial_points sp USING (point_id)
);

-- Find overlapping recordings that happen in the same 'context'
CREATE OR REPLACE VIEW recording_groups AS (
    SELECT
        r1.recording_id AS seed_recording_id,
        r2.recording_id AS overlapping_recording_id
    FROM recordings r1, recordings r2
    WHERE r1.recording_time_range && r2.recording_time_range
        AND r1.recording_id <> r2.recording_id
        AND r1.context_id = r2.context_id
        AND extract_interval(
        r1.recording_time_range * r2.recording_time_range
        ) > interval '10 millisecond'
);

-- Find overlapping time ranges between OE and ACQ files
CREATE OR REPLACE VIEW oe_acq_overlap AS (
       WITH recordings_formats AS (
            SELECT recording_id, recording_time_range, format_name
            FROM recordings
                 INNER JOIN files_recordings USING (recording_id)
                 INNER JOIN files USING (file_id)
                 INNER JOIN formats USING (format_id)
       )
       SELECT s.recording_id AS oe_recording_id,
              s.recording_time_range AS oe_recording_time_range,
              r.recording_id AS ag_recording_id,
              r.recording_time_range AS ag_recording_time_range
       FROM recordings_formats s
            INNER JOIN recordings_formats r ON (r.recording_time_range && s.recording_time_range)
       WHERE s.format_name = 'Open_Ephys_Data_Format-0.4.0-continuous'
       AND r.format_name = 'acquisitiongui_-4'
);
