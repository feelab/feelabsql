BEGIN;

-- File
WITH h AS (
     INSERT INTO hosts (host_name)
     VALUES ('foo')
     RETURNING host_id
), p AS (
   INSERT INTO paths (path_name, host_id)
         SELECT 'bar', host_id
         FROM h
   RETURNING path_id
), f AS (
   INSERT INTO formats (format_name)
   VALUES ('biz')
   RETURNING format_id
)
INSERT INTO files (file_name, path_id, format_id)
SELECT 'ex_file', path_id, format_id
FROM p, f;

-- Recording
WITH dt AS (
INSERT INTO device_types (device_type_name)
VALUES ('a device')
RETURNING device_type_id
), d AS (
INSERT INTO devices (device_type_id, device_name)
SELECT device_type_id, 'the device'
FROM dt
RETURNING device_id
), p AS (
INSERT INTO device_type_pins (device_type_id, pin_name)
SELECT device_type_id, 'a pin'
FROM dt
RETURNING pin_id
), sa AS (
INSERT INTO signal_acquirers (device_id, device_type_id, pin_id)
SELECT device_id, device_type_id, pin_id
FROM d, dt, p
RETURNING acquirer_id
), c AS (
INSERT INTO contexts
VALUES (DEFAULT)
RETURNING context_id
)
INSERT INTO recordings (recording_time_range, context_id, acquirer_id)
SELECT '[2010-01-01 14:30, 2010-01-01 15:30)', context_id, acquirer_id
FROM c, sa;


