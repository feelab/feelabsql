BEGIN;

CREATE TABLE event_detector_types (
detector_type_id    serial PRIMARY KEY,
detector_name       citext UNIQUE NOT NULL -- algorithm
);
CREATE INDEX event_detectors_types_detector_name_index ON
event_detector_types (detector_name text_pattern_ops);

ALTER TABLE event_detectors
DROP CONSTRAINT event_detectors_detector_name_detector_parameters_key;

DROP INDEX event_detectors_detector_name_index;

DROP VIEW IF EXISTS events_with_info;

ALTER TABLE event_detectors
DROP COLUMN detector_name;

ALTER TABLE event_detectors
ADD COLUMN detector_type_id integer REFERENCES event_detector_types NOT NULL;

ALTER TABLE event_detectors
ALTER COLUMN detector_software SET DATA TYPE text;

ALTER TABLE event_detectors
ADD COLUMN detector_version citext;

ALTER TABLE event_detectors
ADD UNIQUE (detector_type_id, detector_software, detector_version, detector_parameters);

CREATE OR REPLACE VIEW events_with_info AS (
WITH event_label_arrays AS(
SELECT e.event_id, array_agg(label_name) AS label_names
FROM events e, events_labels x, labels L
WHERE e.event_id = x.event_id AND x.label_id = L.label_id
GROUP BY e.event_id
)
SELECT *
FROM events LEFT OUTER JOIN event_types USING (type_id)
LEFT OUTER JOIN event_detectors USING (detector_id)
LEFT OUTER JOIN event_detector_types USING (detector_type_id)
LEFT OUTER JOIN recordings_events USING (event_id, context_id)
LEFT OUTER JOIN recordings_with_info USING (recording_id, context_id)
LEFT OUTER JOIN event_label_arrays USING (event_id)
);
-- Has all the columns from events, event_types, event_detectors, and
-- recordings_with_info. It also has a new column called 'label_names' which
-- is an array of label_names associated with each event.
