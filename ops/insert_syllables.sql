BEGIN;

INSERT INTO event_types (type_name, type_description)
VALUES ('syllable', 'zebra finch song syllable');

COMMIT;
