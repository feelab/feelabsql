# FeeLabSQL
SQL database documentation for the Fee Lab

This document describes a relational database schema for storing neurophysiolgy data. Neurophysiology data is a good fit for a relational database because there is a structure to the data. For example, spikes come from recordings, and recordings come from birds. Previously, this infomration was stored across a combination of raw data files recorded in a variety of formats, matlab data files, and excel spreadsheets. With SQL, all of this data is in one place, where it can be cross-referenced and queried quickly and easily.

## Schema description
The database schema is stored in [schema.sql](schema.sql). For a graphical representaion of all of the tables and their connections, look at the [Entity Relationship Diagram](erd.pdf). Open up the diagram and follow along as you read the following plain english description of the schema.

The center of the database is the recordings table. Each recording represents one signal recorded from one bird continuously over some time range. For example, a recording could be audio from a microphone or the voltage trace from one electrode. Each recording is associated with a bird and a source. The raw data for each recording should be in the database in the rawdata table or stored in a separate file listed in the files table.

Sources each have a type, like 'microphone' or 'electrode', and parameters like '{"channel":1,"impedance":"5 MOhm"}'. The source parameters are in the [JSONB format](https://www.postgresql.org/docs/9.5/static/datatype-json.html), which is an indexible format to store [JSON data](http://www.json.org/). The strucuture of the JSON data for each row is unconstrained. It's flexible enough to hold any parameters you might have and it could even have a different strucutre for different sources. However, you should try to keep the structure as consistent as possible across rows so you can query based on the JSON data. For example, always use the key "channel", not "chan" or "ch", so that you can query by channel number using `WHERE source_parameters @> {"channel":2}`. 

Birds are listed in the birds table. Every bird must have an ID number, which is assigned by the lab manager and can be found in the colony spreadsheet. The bird may also have a band (like 'Blue123') and a name (like 'hvcrecording001'). A bird can optionally have additional information that is stored in other tables: about a surgery (surguries table), the date it was separated from its parents (birds_separation table), and the identity of its parents (birds_parentage table).

After recordings, the second most important table is events. Each event has a type like syllable, spike, or stim. Each event spans a time range, comes from a recording and is detected by an event detector. An event may have one or more labels. 

Event types are listed in the event_types table and include 'spike', 'syllable', and 'stim'. Each type should have a short, one word name (type_name). This name is case-insensitive. 

Event detectors should include all the information necessary to reproduce the event timings from the raw data. The detector_name should be the name of a function that did the detection. For example, in electro_gui, this could be 'Spikes AA' for spikes or 'DA Segmenter' for syllables. Detector software should be the software that runs the detector function, for example 'matlab' or 'python'. The column detector_parameters contains any other information necessary about the detector. Like source_parameters, detector_parameters is a JSON object, which means that there is no consistent structure is enforced across detectors. However, your data will be much easier to search if you are consistent. For example, the parameters might be '{"threshold":1.234,"function":"HanningHighPass"}'.

Event labels . They are more specific than event types. For syllables, a label should be a single letter, like 'a', 'b', 'c', etc. For spikes, you can use a single label to mark all the spikes that come from one single unit. Label names are case sensitive. Each label belongs to one bird. That means that syllable 'a' from bird 1 is a differnt label from syllable 'a' in bird 2. The database will not allow you to label an event from one bird with the label of a different bird.

## Getting started

### 1. Install PostgreSQL
Follow the instructions on the [PostgreSQL website](https://www.postgresql.org/download/)

Next, create a new database for your data:

```bash
$ createdb mydb
```

### 2. Import the schema by running schema.sql
From the command line:
```bash
$ psql --dbname=mydb --file=schema.sql
```

### 3. Install the PostgreSQL JDBC
Skip this step if you don't plan to import data from MATLAB.
These instructions were copied from [MATLAB's documentation](http://www.mathworks.com/help/database/ug/postgresql-jdbc-linux.html)
1. Download the [PostgreSQL JDBC driver](https://jdbc.postgresql.org/download.html)
2. In MATLAB, run the `prefdir` command in the Command Window. The output is a file path to a folder on your computer.
3. Close MATLAB
4. Navigate to the folder and create a file called javaclasspath.txt in the folder.
5. Open javaclasspath.txt. Add the full path to the database driver JAR file in javaclasspath.txt. The full path includes the path to the folder where you downloaded the JAR file from the database provider and the JAR file name. For example, /home/user/DB_Drivers/postgresql-8.4-702.jdbc4.jar. Save and close javaclasspath.txt.
6. Restart MATLAB.

### 4. Import your data from MATLAB using copy_dbase_to_postgres.m 
```matlab
>> load example_dbase.mat
>> conn = database('mydb','','',...
    'Vendor','PostgreSQL',...
    'Server','localhost');
>> my_bird = struct('bird_id', 1234);
>> insert_struct(conn, 'birds', my_bird); % add bird before inserting dbase
>> channel_sources = {'', '', 'electrode', 'electrode'};
>> event_types = {'spike', 'spike', 'stim'};
>> hostname = 'tatusobox1';
>> sharename = 'c';
>> copy_dbase_to_postgres(conn, dbase, my_bird.bird_id, ...
    channel_sources, event_types, hostname, sharename)
```
### 5. Query the database! 
In this example, we count the number of recordings for each bird:
```bash
$ psql --dbname mydb <<EOF
\x
SELECT bird_id, COUNT(recording_id) as number_of_recordings
FROM recordings GROUP BY bird_id
EOF
```

[More examples](doc/examples.sql)

## Exploring the Schema

Try [Schema Spy](http://schemaspy.org/), e.g.:
`java -jar ~/Documents/Code/schemaspy/schemaSpy_5.0.0.jar -t pgsql -db galen -host localhost:5433 -u galen -o ~/Documents/Code/schemaspy/output -dp /usr/share/java/postgresql-jdbc3.jar -s public`

The connection details will obviously change from database to database, as will code paths.

## Switching from MATLAB

Analysis in MATLAB usually starts with `electro_gui`. `electro_gui` segments song into syllables and detects spikes and other events. All of that information is stored in a struct called `dbase`, which is usually saved in a file called `analysis.mat`. That `dbase` struct can be imported into the SQL database using the included dbase importer, `copy_dbase_to_postgres.m`.

After using the importer, almost all of the information that was in dbase can now be found in SQL. The table below summarizes the new locations of all of the dbase fields:

| dbase             | SQL                                 |
|-------------------|-------------------------------------|
| PathName          | files.host_name files.path_name     |
| Times             | recordings.recording_time_range     |
| FileLength        | recordings.recording_time_range     |
| SoundFiles        | files.file_name                     |
| ChannelFiles      | files.file_name                     |
| SoundLoader       | files.format                        |
| ChannelLoader     | files.format                        |
| Fs                | rawdata.sample_time                 |
| SegmentThresholds | event_detectors.detector_parameters |
| SegmentTimes      | events.event_time_range             |
| SegmentTitles     | events_labels.*                     |
| SegmentIsSelected | events.passed_review                |
| EventSources      | events.recording_id                 |
| EventFunctions    | event_detectors.detector_parameters |
| EventDetectors    | event_detectors.detector_name       |
| EventThresholds   | event_detectors.detector_parameters |
| EventTimes        | events.event_time_range             |
| EventIsSelected   | events.passed_inspection            |
| Properties        | recordings_notes.recording_note     |
| AnalysisState     | NOT INCLUDED                        |

