-- ------------------------------------------------------------------------- --
-- ------------------------------------------------------------------------- --
--                                                                           --
--                          Examples for Fee Lab SQL                         --
--                                                                           --
-- ------------------------------------------------------------------------- --
-- ------------------------------------------------------------------------- --

    
-- Stim response for a unit
-- (raw data aligned to events)

WITH stim AS (
    SELECT event_id, lower(event_time_range) as onset, bird_id
        FROM events_with_info
        WHERE type_name = 'stim'
)
SELECT stim.event_id AS stim_id,
       extract(epoch FROM (sample_time - stim.onset)) AS seconds_from_stim,
       data_value,
    FROM stim, rawdata INNER JOIN recordings USING (recording_id) AS d 
    WHERE d.bird_id = 6155
      AND d.bird_id = stim.bird_id
      AND stim.event_time_range <@ tsrange('2015-03-20 13:42', '2015-03-20 14:04')
      AND abs(extract(epoch FROM (sample_time - stim.onset))) < 0.03 -- within 30 ms

-- Choose the right stim pulses using the bird_id and the event_time_range criteria.
      
-- Note: Do NOT write a query like 'WHERE sample_time <@ event_time_range'. 
--       For some reason, that syntax is much slower!


-- ------------------------------------------------------------------------- --


-- How many single units have I recorded in my experiment?

-- TODO


-- ------------------------------------------------------------------------- --    


-- List juvenile birds I recorded for my experiment
-- (Only include birds that have spikes)

-- TODO

-- ------------------------------------------------------------------------- --


-- Spike times aligned to syllable onset
-- (events aligned to events)
WITH spike AS (
    SELECT event_id, lower(event_time_range) as t
        FROM events_with_info
        WHERE source_parameters @> '{"channel": 3}'
          AND event_time_range <@ tsrange('2015-03-20 13:42', '2015-03-20 14:04')
          AND bird_id = 5640
), syll AS (
    SELECT event_id, lower(event_time_range) as t 
        FROM events INNER JOIN events_labels USING (event_id)
                    INNER JOIN labels USING (label_id)
        WHERE label_name = 'a'
)
SELECT syll.event_id AS syll_id, 
       spike.event_id AS spike_id, 
       spike.t - syll.t AS dt
FROM spike, syll
WHERE abs(extract(epoch FROM (spike.t - syll.t))) < 1 -- within 1 second


-- ------------------------------------------------------------------------- --


-- Get all data files that are in my paper

-- TODO

