BEGIN;

-- Change unique constraint on paths form just pathname to host and path name
ALTER TABLE paths DROP CONSTRAINT paths_path_name_key;
ALTER TABLE paths ADD CONSTRAINT paths_host_id_path_name_key UNIQUE (host_id, path_name);
CREATE INDEX paths_name_index ON paths (path_name);

-- Add unique constraint to files
ALTER TABLE files ADD CONSTRAINT files_file_name_path_id_key UNIQUE (file_name, path_id);
DROP INDEX IF EXISTS files_file_name_idx;

COMMIT;
