BEGIN;

DROP TRIGGER IF EXISTS event_recording_existence ON events;

DROP FUNCTION IF EXISTS event_recording_existence();


CREATE OR REPLACE FUNCTION events_no_orphan_trg()
RETURNS trigger AS $events_no_orphan_trg$
BEGIN
IF (event_is_orphan(NEW.event_id)) THEN
RAISE EXCEPTION
'Event with event_id % is not referenced in recordings_events',
NEW.event_id;
END IF;
RETURN NEW;
END;
$events_no_orphan_trg$ LANGUAGE plpgsql;

CREATE CONSTRAINT TRIGGER events_no_orphan_events_trg
AFTER INSERT OR UPDATE ON events
DEFERRABLE INITIALLY DEFERRED
FOR EACH ROW EXECUTE PROCEDURE events_no_orphan_trg();


COMMIT;
